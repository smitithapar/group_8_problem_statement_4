from django.shortcuts import render, get_object_or_404
from .models import Category, Product, Service, Support
from cart.forms import CartAddProductForm
from shop.forms import FeedbackForm
import pandas as pd


def product_list(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    current_user = request.user.userprofile
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request,
                  'shop/product/list.html',
                  {'category': category,
                   'categories': categories,
                   'products': products,
                   'current_user': current_user})


def product_detail(request, id, slug):
    product = get_object_or_404(Product,
                                id=id,
                                slug=slug,
                                available=True)
    cart_product_form = CartAddProductForm()
    return render(request,
                  'shop/product/detail.html',
                  {'product': product,
                   'cart_product_form': cart_product_form})


def service_detail(request, id):
    service = get_object_or_404(Service, id=id)

    cart_product_form = CartAddProductForm()
    return render(request,
                  'shop/product/service_detail.html',
                  {'service': service,
                   'cart_product_form': cart_product_form})


def support_page(request):
    supports = Support.objects.all()

    return render(request, 'shop/product/support.html', {'supports': supports})


# todo
def service_render(request, product_id):
    print("Product ID : " + str(product_id))
    services = Service.objects.all()
    recommended_services = []
    for service in services:
        print("Service Product ID " + str(service.product.product_id) + " Product ID " + str(product_id))
        if service.product.product_id == product_id:
            print(str(service.product.product_id) + " " + str(product_id))
            recommended_services.append(service)
    print("Recommended Services " + str(recommended_services))
    user_profile_id = request.user.userprofile.custom_user_id
    print("User Profile ID : " + str(user_profile_id))

    return render(request, 'shop/product/services-.html', {'services': recommended_services})


def feedback_form(request):
    feedback_submitted = False
    if request.method == 'POST':
        feedback_form = FeedbackForm(data=request.POST)
        if feedback_form.is_valid():
            feedback_detail = feedback_form.save()
            feedback_detail.save()
            feedback_submitted = True
        # Not a HTTP POST, so we render our form using two ModelForm instances.
        # These forms will be blank, ready for user input.
    else:
        feedback_form = FeedbackForm()
    return render(request, 'shop/product/feedback.html',
                  {'feedback_form': feedback_form, 'feedback_submitted': feedback_submitted})


def service_purchased(request):
    current_user = request.user.userprofile
    print("Current User : " + str(current_user.conversion_rate))
    if current_user.conversion_rate == 0:
        current_user.conversion_rate = 1
    request.user.userprofile.save()
    return render(request, 'shop/product/service_purchased.html', {})


def affinity(user, userCount, itemList):
    itemAffinity = pd.DataFrame(columns=('p_id1', 'p_id2', 'score'))
    rowCount = 0

    # For each item in the list, compare with other items.
    for i in range(len(itemList)):

        # Get list of users
        item1Users = user[user.p_id == itemList[i]]["user_id"].tolist()

        # Get item 2 - items that are not item 1 or those that are not analyzed already.
        for j in range(i, len(itemList)):

            if i == j:
                continue

            # Get list of users who bought item 2
            item2Users = user[user.p_id == itemList[j]]["user_id"].tolist()

            # Find score.
            common_users = len(set(item1Users).intersection(set(item2Users)))
            score = common_users / userCount

            # Add a score for item 1, item 2
            itemAffinity.loc[rowCount] = [itemList[i], itemList[j], score]
            rowCount += 1
            # Add a score for item2, item 1
            itemAffinity.loc[rowCount] = [itemList[i], itemList[j], score]
            rowCount += 1

    return itemAffinity


def test_func(user):
    itemList = list(set(user["p_id"].tolist()))  # unique items
    count = 5

    itemAffinity = affinity(user, count, itemList)
    searchItem = int(input("Product id: "))
    recoList = itemAffinity[itemAffinity.p_id1 == searchItem] \
        [["p_id2", "score"]] \
        .sort_values("score", ascending=[0])

    print("Recommendations for item:", searchItem, '')
    # print(searchItem)
    threshold = 0.5
    while threshold:
        D = recoList[recoList["score"] > threshold]
        if D.empty:
            threshold = threshold / 2
            continue
        break;
    # print(D["p_id2"].unique())
    for i in D["p_id2"].unique():
        print(int(i))

    product = pd.read_excel('%04d.xlsx' % (searchItem))
    SList = list(set(product["p_id"].tolist()))
    ServiceAffinity = affinity(product, count, SList)
    threshold = 0.5
    while (threshold):
        E = ServiceAffinity[ServiceAffinity["score"] > threshold]
        if (E.empty):
            threshold = threshold / 2
            continue
        break;

    print("Recommendations for Services of product :", searchItem, '')
    for i in E["p_id2"].unique():
        print(int(i))
    for i in E["p_id1"].unique():
        print(int(i))
